class Post < ActiveRecord::Base
  has_many :comments, dependent: :destroy
  validates_presents_of :title
  validates_presents_of :body
end
